# Visual Companion Database

A simple and easy way to make a plaintext database with a GUI editor built in rust.

## Installation

Currently in development, feel free to star and come back later when we're ready for you :)

## Usage

This project aims to be a super customizable personalized plaintext database just for you. Ideally this project allows you to store whatever kind of information you want and to gain key insights from graphs and relationships between entries. Ideal for keeping track of anything.

## Contributing

This project is built on rust. Before contributing: setup a rust hello world program on your computer.

This project is under **strict** [test-driven development](https://www.scaleagileframework.com/test-driven-development). All code must pass a reasonable test before being committed. Thank you for your patience.

```cargo clippy``` is your friend :)

Please contact me if you know what you are doing and still can't get it to work.

## Authors and acknowledgment
People who have put a considerable amount of work into this project:

+ [Cameron Dugan](https://camerondugan.com)
+ \<Insert Your Name Here\>

## License
This software is licensed under the [GNU GPL Version 3](https://choosealicense.com/licenses/gpl-3.0/).