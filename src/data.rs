pub struct Data {
    pub name: String,
    pub relationships: Option<Vec<Data>>,
    pub data: Option<Vec<String>>,
}

pub fn from(name: String) -> Data {
    Data { name, relationships: None, data: None }
}

//unimplemented
pub fn import_from_file(file: String) {
}

//unimplemented
pub fn import_all_from_folder(folder: String) {
}