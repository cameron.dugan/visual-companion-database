use glib::clone;
use gtk::glib;
use gtk::prelude::*;

pub fn start() {
    let app = gtk::Application::builder()
        .application_id("com.camerondugan.visual-companion-databases")
        .build();

    app.connect_activate(on_activate);

    app.run();
}

fn on_activate(app: &gtk::Application) {

    // The main window.
    let window = gtk::ApplicationWindow::builder()
        .application(app)
        .default_width(666)
        .default_height(666)
        .title("Kanban Board")
        .build();
    
    // Side pane container
    let paned_window = gtk::Paned::new(gtk::Orientation::Horizontal);
    paned_window.set_position(150);

    // Construct side_pane
    let hello_button = gtk::Button::with_label("Hello Kanban");
    let hello_button2 = gtk::Button::with_label("Hello Kanban");
    hello_button.connect_clicked(|_| {
            eprintln!("Hello Button Clicked!");
    });
    hello_button.set_has_frame(false);
    let side_pane = gtk::Box::new(gtk::Orientation::Vertical,0);
    side_pane.append(&hello_button);

    //left pane
    paned_window.set_start_child(&side_pane);
    //right pane
    paned_window.set_end_child(&hello_button2);

    window.set_child(Some(&paned_window));
    window.show();
}