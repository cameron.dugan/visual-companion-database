mod gui;
mod data;

fn main() {
    println!("Hello, world!");
    gui::start();
}

#[cfg(test)]
mod tests {
    use crate::data;

    #[test]
    fn sanity_check() {
        assert_eq!(2+2,4);
    }

    #[test]
    fn test_data_class() {
        let entry_data = Vec::from(["Hello".to_owned(),"World".to_owned()]);
        let mut data_test = data::from(String::from("Hello"));
        data_test.data = Some(entry_data.clone());
        assert_eq!(data_test.name,String::from("Hello"));
        assert!(data_test.relationships.is_none());
        assert_eq!(data_test.data,Some(entry_data));
    }
}